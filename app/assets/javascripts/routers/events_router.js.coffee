class EatEatEat.Routers.Events extends Backbone.Router

  routes:
    '' : 'index'
    'new': 'new'
    ':id' : 'show'
    'user/:id': 'myindex'

  initialize: ->
    @collectionlist ={
      users: new EatEatEat.Collections.Users()
      events: new EatEatEat.Collections.Events()
    }
    for key, value of @collectionlist
      @collectionlist[key].fetch({'reset': true})

  index: ->
    view = new EatEatEat.Views.EventsIndex(el: '#container', collection: @collectionlist)
    view.render()

  new: ->
    view = new EatEatEat.Views.EventsNew(el: "#container")
    view.render()

  show: (id)->
    alert 'show/' + id
  myindex: (id) ->
    view = new EatEatEat.Views.MyEventsIndex(el: '#container', collection: @collectionlist, id: id)
    view.render().el
