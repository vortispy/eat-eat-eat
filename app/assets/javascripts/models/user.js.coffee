class EatEatEat.Models.User extends Backbone.Model
  url: "/api/users"
  defaults:
    name: ''
    icon_url: ''
    email: ''
    token: null
    profile_url: ''
    created_at: new Date
    updated_at: new Date

  validate: (attrs) ->
    console.log 'do validate!'
    if attrs.hasOwnProperty('created_at') and !_.isDate(attrs.created_at)
      'Event.created_at must be a Date object.'
