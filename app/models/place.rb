class Place < ActiveRecord::Base
  attr_accessible :latlong, :name

  has_many :events
end
