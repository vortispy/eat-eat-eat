# # coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'json'
require 'activerecord-import'

# create user
users_list = JSON.parse File.read('db/users.json')
users = User.create(users_list)

# create places
places_srouce = JSON.parse File.read("db/places.json")
places_list = [{name: "ギークハウス新潟", latlong:""}]
places_list += places_srouce.map{|m|
   loc = m['location']
   {name:m['name'], latlong:"#{loc['latitude']},#{loc['longitude']}" }
}
places = Place.create(places_list)

# create events
titles = %w(
カレー食べよう
焼き肉食べよう
ラーメン食べよう
)
events = 100.times.map{
   date = Date.today + (15 - rand(30))
   event = Event.new(title: titles.sample(1), schedule_for: date)
   event.place = places.sample(1).first
   event.owner = users.sample(1).first
   event.save
   event
}


entries = events.map do |event|
   event.entrants = users.sample( 1 + rand(5) )
end

=begin
stories = Activity::Story.create([
   { title:"ダミー", content:"この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。" },
   { title:"ぼっちゃん", content:"親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。（青空文庫より）" },
   { title:"つれづれ", content:"つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。（Wikipediaより）"},
   { title:"ダミーテキスト", content:"ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテ" },

   { title:"高尾山", content:"この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。" },
   { title:"ボッチャン", content:"親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。（青空文庫より）" },
   { title:"徒然草", content:"つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。（Wikipediaより）"},
   { title:"タイトル", content:"ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテキスト。ダミーテ" }
])

%w(
エクストリーム アイロン掛け
沼津漁港を満喫する旅
高尾山を登ってからの～
犬ぞり／イヌぞり
サーフィン／ボディボード
梨狩り
筑波山に挑戦します
市ヶ谷釣り堀デビュー
都内で温泉
).each do |title|
   a = Activity.create(title:title, owner_id:1)
   stories.sort_by{rand}[0,5].each{|s| s.activity = a}
end
=end
