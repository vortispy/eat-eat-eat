require 'spec_helper'

describe EventsController do

  # This should return the minimal set of attributes required to create a valid
  # Event. As you add validations to Event, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "title" => "MyString", :schedule_for => Date.new }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # EventsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before do
    request.accept = 'application/json'
  end

  describe "GET" do
     it "response json" do
	get :index
	header = response.header
	expect(header["Content-Type"]).to match /application\/json/
     end
  end

  describe "GET index" do
    it "assigns all events as @events" do
      event = Event.create! valid_attributes
      a = get :index, {}, valid_session
      json = response.body
      expect(json).to eq [event].to_json
    end
  end

  describe "GET show" do
    it "assigns the requested event as @event" do
      event = Event.create! valid_attributes
      get :show, {:id => event.to_param}, valid_session
      json = response.body
      expect(json).to eq event.to_json
    end
  end


  describe "POST create" do
    describe "with valid params" do
      it "creates a new Event" do
        expect {
          post :create, {:event => valid_attributes}, valid_session
        }.to change(Event, :count).by(1)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved event as @event" do
        # Trigger the behavior that occurs when invalid params are submitted
        Event.any_instance.should_receive(:errors).at_least(1).and_return({"title" => "is invalid"})
        Event.any_instance.should_receive(:valid?).at_least(1).and_return(false)
        post :create, {:event => { "title" => "invalid value" }}, valid_session
	expect(response.status).to eq 422
	errors = JSON.parse(response.body)['errors']
	expect(errors).to have_key("title")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested event" do
        event = Event.create! valid_attributes
        # Assuming there are no other events in the database, this
        # specifies that the Event created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Event.any_instance.should_receive(:update_attributes).with({ "title" => "MyString" })
        put :update, {:id => event.to_param, :event => { "title" => "MyString" }}, valid_session
	expect(response.body.blank?).to be_true
      end
    end

    describe "with invalid params" do
      it "can't update" do
        event = Event.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        #Event.any_instance.stub(:save).and_return(false)
        Event.any_instance.should_receive(:errors).at_least(1).and_return({"title" => "is invalid"})
        Event.any_instance.should_receive(:valid?).at_least(1).and_return(false)
        put :update, {:id => event.to_param, :event => { "title" => "invalid value" }}, valid_session
	expect(response.status).to eq 422
	errors = JSON.parse(response.body)['errors']
	expect(errors).to have_key("title")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested event" do
      event = Event.create! valid_attributes
      expect {
        delete :destroy, {:id => event.to_param}, valid_session
      }.to change(Event, :count).by(-1)
    end
  end
end
