require 'spec_helper'

describe UsersController do

  # This should return the minimal set of attributes required to create a valid
  # User. As you add validations to User, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "name" => "MyString", :icon_url => '/avator/1.png' }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # UsersController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  before do
    request.accept = 'application/json'
  end

  describe "GET" do
     it "response json" do
	get :index
	header = response.header
	expect(header["Content-Type"]).to match /application\/json/
     end
  end

  describe "GET index" do
    it "assigns all users as @users" do
      user = User.create! valid_attributes
      a = get :index, {}, valid_session
      json = response.body
      expect(json).to eq [user].to_json
    end
  end

  describe "GET show" do
    it "assigns the requested user as @user" do
      user = User.create! valid_attributes
      get :show, {:id => user.to_param}, valid_session
      json = response.body
      expect(json).to eq user.to_json
    end
  end


  describe "POST create" do
    describe "with valid params" do
      it "creates a new User" do
        expect {
          post :create, {:user => valid_attributes}, valid_session
        }.to change(User, :count).by(1)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved user as @user" do
        # Trigger the behavior that occurs when invalid params are submitted
        User.any_instance.should_receive(:errors).at_least(1).and_return({"name" => "is invalid"})
        User.any_instance.stub(:valid?).and_return(false)
        post :create, {:user => { "name" => "invalid value" }}, valid_session
	expect(response.status).to eq 422
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested user" do
        user = User.create! valid_attributes
        # Assuming there are no other users in the database, this
        # specifies that the User created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        User.any_instance.should_receive(:update_attributes).with({ "name" => "MyString" })
        put :update, {:id => user.to_param, :user => { "name" => "MyString" }}, valid_session
	expect(response.body.blank?).to be_true
      end
    end

    describe "with invalid params" do
      it "can't update" do
        user = User.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        #User.any_instance.stub(:save).and_return(false)
        User.any_instance.should_receive(:errors).at_least(1).and_return({"name" => "is invalid"})
        User.any_instance.stub(:valid?).and_return(false)
        put :update, {:id => user.to_param, :user => { "name" => "invalid valude" }}, valid_session
	expect(response.status).to eq 422
	errors = JSON.parse(response.body)['errors']
	expect(errors).not_to be_nil
	expect(errors).to have_key("name")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested user" do
      user = User.create! valid_attributes
      expect {
        delete :destroy, {:id => user.to_param}, valid_session
      }.to change(User, :count).by(-1)
    end
  end
end
